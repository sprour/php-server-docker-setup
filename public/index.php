<?php
$servername = "mysql";
$username = "root";
$password = "secret";

// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
    die("Упс, что-то пошло не так! К базе не удалось подключиться: " . $conn->connect_error);
}

echo "<div style='text-align: center'>";

echo "<h1>Ура! Работает!</h1>";

echo "<p>Теперь можно писать код, у тебя обязательно все получится!</p>";

echo "<p>Для доступа к базе данных MySQL перейди в <a href='http://localhost:81' target='_blank'>phpMyAdmin</a></p>";
echo "<p>Server: mysql</p>";
echo "<p>Username: root</p>";
echo "<p>Password: secret</p>";


echo "</div>";
